import { Navigation } from 'react-native-navigation';

import { Provider } from 'react-redux';

import AuthScreen from './src/screens/Auth/Auth';
import IntroSlider from './src/screens/IntroSlider/IntroSlider';
import SharePlaceScreen from './src/screens/SharePlace/SharePlace';
import FindPlaceScreen from './src/screens/FindPlace/FindPlace';
import PlaceDetail from './src/screens/PlaceDetail/PlaceDetail';

import configureStore from './src/store/configureStore';

const store = configureStore();

//Register Screens
Navigation.registerComponent(
    "Awesome-places.AuthScreen",
    () => AuthScreen,
    store,
    Provider);
Navigation.registerComponent(
    "Awesome-places.IntroSlider",
    () => IntroSlider,
    store,
    Provider);
Navigation.registerComponent(
    "Awesome-places.SharePlaceScreen",
    () => SharePlaceScreen,
    store,
    Provider);
Navigation.registerComponent(
    "Awesome-places.FindPlaceScreen",
    () => FindPlaceScreen,
    store,
    Provider);
Navigation.registerComponent(
    "Awesome-places.PlaceDetail",
    () => PlaceDetail,
    store,
    Provider);

//Start a App
Navigation.startSingleScreenApp({
    screen: {
        screen: "Awesome-places.IntroSlider",
        title: "IntroSlider", 
        navigatorStyle: {
            navBarHidden: true
        },
        statusBarTextColorScheme: 'light',
        statusBarTextColorSchemeSingleScreen: 'light',
    }
});