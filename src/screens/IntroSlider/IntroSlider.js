import React, { Component } from 'react';
import { Dimensions, StyleSheet, View, Text, Button, TouchableWithoutFeedback, StatusBar } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

const { width } = Dimensions.get('window');

import startMainTabs from '../MainTabs/startMainTabs';

class AuthScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showRealApp: false
        };
    }

    loginHandler = () =>{
        startMainTabs();
    }

    _onDone = () => {
        this.setState({ showRealApp: true });
    };

    render() {
        if (this.state.showRealApp) {
            return (
                <View style={styles.mainContent}>
                    <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#F6F7F8" translucent = {true}/>
                    <Text>Auth Screen</Text>
                    <Button
                        title="Login"
                        onPress={this.loginHandler} />
                </View>
            );
        }
        else {
            return (
                <View style={styles.mainContent}>
                    <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#FFFFFF" translucent = {true}/>
                    <AppIntroSlider
                        slides={slides}
                        onDone={this._onDone}
                        showSkipButton={false}
                        onSkip={this._onSkip}
                    />
                    <TouchableWithoutFeedback
                        onPress={this._onDone}>
                        <View style={styles.placeTouchableWithoutFeedbackAsButton}>
                            <Text style={styles.textWhite}>Get Started</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            );
        }
    }
}


const styles = StyleSheet.create({
    mainContent: {
        flex: 1
    }, image: {
        width: width - 50,
        height: width - 50,
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'black',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginTop: 16,
    },
    text: {
        fontSize: 13,
        color: 'black',
        marginLeft: 15,
        marginRight: 15,
    },
    placeTouchableWithoutFeedbackAsButton: {
        backgroundColor: 'rgba(255,130,66,1)',
        borderRadius: 5,
        borderWidth: 0.5,
        borderColor: 'rgba(255,130,66,1)',
        marginLeft: 9,
        marginBottom: 9,
        marginRight: 9,
        padding: 9
    },
    textWhite: {
        textAlign: 'center',
        fontSize: 16,
        color: 'white'
    }
});

const slides = [
    {
        key: 's1',
        title: 'All your stuff at one place',
        titleStyle: styles.title,
        text: 'In my capacity as the UK Director of Operactions for One World Tours Limited, one of my jobs is to ensure every client has the best tour.',
        textStyle: styles.text,
        image: {
            uri: 'http://192.168.0.189:8081/src/assets/slider_image.png',
        },
        imageStyle: styles.image,
        backgroundColor: '#FFFFFF',
    }, {
        key: 's2',
        title: 'All your stuff at one place',
        titleStyle: styles.title,
        text: 'In my capacity as the UK Director of Operactions for One World Tours Limited, one of my jobs is to ensure every client has the best tour.',
        textStyle: styles.text,
        image: {
            uri: 'http://192.168.0.189:8081/src/assets/slider_image.png',
        },
        imageStyle: styles.image,
        backgroundColor: '#FFFFFF',
    }, {
        key: 's3',
        title: 'All your stuff at one place',
        titleStyle: styles.title,
        text: 'In my capacity as the UK Director of Operactions for One World Tours Limited, one of my jobs is to ensure every client has the best tour.',
        textStyle: styles.text,
        image: {
            uri: 'http://192.168.0.189:8081/src/assets/slider_image.png',
        },
        imageStyle: styles.image,
        backgroundColor: '#FFFFFF',
    }
];

export default AuthScreen;