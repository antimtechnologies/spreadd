import React, {Component} from 'react';
import {View} from 'react-native';
import { connect } from 'react-redux';

import PlaceListForSelect from '../../components/PlaceList/PlaceListForSelect';

class FindPlaceScreen extends Component{

    itemSelectedHandler = key =>{
        const selPlace = this.props.places.find(place =>{
            return place.key == key;
        });
        this.props.navigator.push({
            screen: "Awesome-places.PlaceDetail",
            title: selPlace.value,
            passProps:{
                selectedPlace: selPlace
            }
        });
    }

    render() {
        return(
            <View>
                <PlaceListForSelect places={this.props.places} onItemSelected={this.itemSelectedHandler}/>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        places: state.places.places
    };
};

export default connect(mapStateToProps)(FindPlaceScreen);