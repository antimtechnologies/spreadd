import React, {Component} from 'react';
import {View, Text} from 'react-native';
import { connect } from 'react-redux';

import PlaceInput from '../../components/PlaceInput/PlaceInput';
import { addPlace } from '../../store/actions/index';

class SharePlaceScreen extends Component{

    placeAddedHandler = (placeName,ImageLink) =>{
        this.props.onAddPlace(placeName,ImageLink);
    }

    render() {
        return(
            <View>
                <PlaceInput onPlaceAdded={this.placeAddedHandler} />
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddPlace: (placeName, ImageLink) => dispatch(addPlace(placeName,ImageLink))
    };
};

export default connect(null,mapDispatchToProps)(SharePlaceScreen);