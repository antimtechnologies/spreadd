import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput, TouchableWithoutFeedback } from 'react-native';

class PlaceInput extends Component {

    state = {
        placeName: "",
        ImageLink: ""
    };

    placeNameChangedHandler = val => {
        this.setState({
            placeName: val
        });
    };

    ImageChangedHandler = val => {
        this.setState({
            ImageLink: val
        });
    };

    placeSubmitHandler = () => {
        if (this.state.placeName.trim() === "" || this.state.ImageLink.trim() === "") {
            return;
        }

        this.props.onPlaceAdded(this.state.placeName, this.state.ImageLink);

        // this.setState({
        //     placeName: '',
        //     ImageLink: ''
        // });
    }

    render() {
        return (
            <View style={styles.containerView}>
                {/* <View style={styles.inputContainer}>
                    <TextInput
                        placeholder="An awesome place"
                        value={this.state.placeName}
                        onChangeText={this.placeNameChangedHandler}
                        style={styles.placeInput}
                    />
                    <Button
                        title="Add"
                        style={styles.placeButton}
                        onPress={this.placeSubmitHandler} />
                </View> */}
                <TextInput
                    placeholder="An awesome place"
                    value={this.state.placeName}
                    onChangeText={this.placeNameChangedHandler}
                    style={styles.placeInput1}
                />
                <TextInput
                    placeholder="Image Link"
                    value={this.state.ImageLink}
                    onChangeText={this.ImageChangedHandler}
                    style={styles.placeInput1}
                />
                <TouchableWithoutFeedback
                    onPress={this.placeSubmitHandler}>
                    <View style={styles.placeTouchableWithoutFeedbackAsButton}>
                        <Text style={styles.textWhite}>Add</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerView: {
        width: "100%",
        padding: 26
    },
    inputContainer: {
        //flex: 1,
        width: "100%",
        marginBottom: 5,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    placeInput: {
        width: "70%",
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da'
    },
    placeInput1: {
        width: "100%",
        borderRadius: 4,
        borderWidth: 0.5,
        marginBottom: 10,
        borderColor: '#d6d7da'
    },
    placeButton: {
        width: "30%"
    },
    placeTouchableWithoutFeedbackAsButton: {
        backgroundColor: '#2196F3',
        borderRadius: 30,
        borderWidth: 0.5,
        borderColor: 'white',
        marginLeft: 9,
        marginBottom: 9,
        marginRight: 9,
        padding: 9
    },
    textWhite: {
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        color: 'white'
    }
});

export default PlaceInput;