import React from 'react';
import { FlatList, StyleSheet } from 'react-native';

import ListItem from '../ListItem/ListItem';

const PlaceListForDelete = props => {

    return (
        <FlatList style={styles.listContainer}
            data={props.places}
            renderItem={(info) => (
                <ListItem
                    placeName={info.item.value}
                    placeImag={info.item.image}
                    //onItemPressed={() => alert('Item pressed - ID: ' + info.item.key)}
                    onItemPressed={() => props.onItemDeleted(info.item.key)}
                />
            )}
        />
    );
};

const styles = StyleSheet.create({
    listContainer: {
        width: "100%"
    }
});

export default PlaceListForDelete;