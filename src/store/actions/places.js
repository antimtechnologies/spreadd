import { ADD_PLACE, DELETE_PLACE } from './actionType';

export const addPlace = (placeName, image) => {
    return {
        type: ADD_PLACE,
        placeName: placeName,
        ImageLink: image
    };
};

export const deletePlace = (key) => {
    return {
        type: DELETE_PLACE,
        key: key
    };
};