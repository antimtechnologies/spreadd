import { ADD_PLACE, DELETE_PLACE} from "../actions/actionType";

const initState = {
    places: [],
    selectedPlace: null
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_PLACE:
            alert("Record add Sccessfull");
            return {
                ...state,
                places: state.places.concat({                    
                    key: Math.random().toString(),
                    value: action.placeName,
                    image: {
                        uri: action.ImageLink
                    }
                })
            };
        case DELETE_PLACE:
            return {
                ...state,
                places: state.places.filter((place, i) => {
                    return i !== action.key;
                })
            };
        default:
            return state
    }
}

export default reducer;